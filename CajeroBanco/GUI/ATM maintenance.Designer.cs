﻿namespace CajeroBanco.GUI
{
    partial class ATM_maintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newB = new System.Windows.Forms.ToolStripButton();
            this.Save = new System.Windows.Forms.ToolStripButton();
            this.cancel = new System.Windows.Forms.ToolStripButton();
            this.delete = new System.Windows.Forms.ToolStripButton();
            this.txtDeno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgDenominatios = new System.Windows.Forms.DataGridView();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDenominatios)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(16, 18);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newB,
            this.Save,
            this.cancel,
            this.delete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(402, 40);
            this.toolStrip1.TabIndex = 21;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newB
            // 
            this.newB.Image = global::CajeroBanco.Properties.Resources.document_new_6;
            this.newB.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.newB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newB.Name = "newB";
            this.newB.Size = new System.Drawing.Size(46, 37);
            this.newB.Text = "Nuevo";
            this.newB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.newB.Click += new System.EventHandler(this.newB_Click);
            // 
            // Save
            // 
            this.Save.Image = global::CajeroBanco.Properties.Resources.document_save_5;
            this.Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(53, 37);
            this.Save.Text = "Guardar";
            this.Save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // cancel
            // 
            this.cancel.Image = global::CajeroBanco.Properties.Resources.dialog_cancel_4;
            this.cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(57, 37);
            this.cancel.Text = "Cancelar";
            this.cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // delete
            // 
            this.delete.Image = global::CajeroBanco.Properties.Resources.edit_delete_9;
            this.delete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(54, 37);
            this.delete.Text = "Eliminar";
            this.delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // txtDeno
            // 
            this.txtDeno.Location = new System.Drawing.Point(136, 53);
            this.txtDeno.Name = "txtDeno";
            this.txtDeno.Size = new System.Drawing.Size(121, 20);
            this.txtDeno.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Denominacion";
            // 
            // dgDenominatios
            // 
            this.dgDenominatios.AllowUserToAddRows = false;
            this.dgDenominatios.AllowUserToDeleteRows = false;
            this.dgDenominatios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDenominatios.Location = new System.Drawing.Point(66, 90);
            this.dgDenominatios.Name = "dgDenominatios";
            this.dgDenominatios.ReadOnly = true;
            this.dgDenominatios.Size = new System.Drawing.Size(182, 144);
            this.dgDenominatios.TabIndex = 24;
            this.dgDenominatios.Click += new System.EventHandler(this.dataDenominatios_Click);
            // 
            // ATM_maintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 310);
            this.Controls.Add(this.dgDenominatios);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDeno);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ATM_maintenance";
            this.Text = "ATM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ATM_maintenance_FormClosing);
            this.Load += new System.EventHandler(this.ATM_maintenance_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDenominatios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton Save;
        private System.Windows.Forms.ToolStripButton cancel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TextBox txtDeno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton newB;
        private System.Windows.Forms.ToolStripButton delete;
        private System.Windows.Forms.DataGridView dgDenominatios;
    }
}