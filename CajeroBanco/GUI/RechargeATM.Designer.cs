﻿namespace CajeroBanco.GUI
{
    partial class RechargeATM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.cancel = new System.Windows.Forms.ToolStripButton();
            this.delete = new System.Windows.Forms.ToolStripButton();
            this.dgDenominatios = new System.Windows.Forms.DataGridView();
            this.numericEdit = new System.Windows.Forms.NumericUpDown();
            this.numericNew = new System.Windows.Forms.NumericUpDown();
            this.cbxDenominatios = new System.Windows.Forms.ComboBox();
            this.lblClick = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDenominatios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNew)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(16, 18);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnSave,
            this.cancel,
            this.delete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(422, 40);
            this.toolStrip1.TabIndex = 22;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.Image = global::CajeroBanco.Properties.Resources.document_new_6;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(46, 37);
            this.btnNew.Text = "Nuevo";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNew.Click += new System.EventHandler(this.newB_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::CajeroBanco.Properties.Resources.document_save_5;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(53, 37);
            this.btnSave.Text = "Guardar";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSave.Click += new System.EventHandler(this.Save_Click);
            // 
            // cancel
            // 
            this.cancel.Image = global::CajeroBanco.Properties.Resources.dialog_cancel_4;
            this.cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(57, 37);
            this.cancel.Text = "Cancelar";
            this.cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // delete
            // 
            this.delete.Image = global::CajeroBanco.Properties.Resources.edit_delete_9;
            this.delete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(54, 37);
            this.delete.Text = "Eliminar";
            this.delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // dgDenominatios
            // 
            this.dgDenominatios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDenominatios.Location = new System.Drawing.Point(41, 113);
            this.dgDenominatios.Name = "dgDenominatios";
            this.dgDenominatios.Size = new System.Drawing.Size(287, 109);
            this.dgDenominatios.TabIndex = 23;
            this.dgDenominatios.Click += new System.EventHandler(this.dgDenominatios_Click);
            // 
            // numericEdit
            // 
            this.numericEdit.Location = new System.Drawing.Point(170, 84);
            this.numericEdit.Name = "numericEdit";
            this.numericEdit.Size = new System.Drawing.Size(120, 20);
            this.numericEdit.TabIndex = 24;
            // 
            // numericNew
            // 
            this.numericNew.Location = new System.Drawing.Point(170, 43);
            this.numericNew.Name = "numericNew";
            this.numericNew.Size = new System.Drawing.Size(120, 20);
            this.numericNew.TabIndex = 25;
            // 
            // cbxDenominatios
            // 
            this.cbxDenominatios.FormattingEnabled = true;
            this.cbxDenominatios.Location = new System.Drawing.Point(29, 43);
            this.cbxDenominatios.Name = "cbxDenominatios";
            this.cbxDenominatios.Size = new System.Drawing.Size(121, 21);
            this.cbxDenominatios.TabIndex = 26;
            // 
            // lblClick
            // 
            this.lblClick.AutoSize = true;
            this.lblClick.Location = new System.Drawing.Point(57, 84);
            this.lblClick.Name = "lblClick";
            this.lblClick.Size = new System.Drawing.Size(0, 13);
            this.lblClick.TabIndex = 28;
            // 
            // RechargeATM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 261);
            this.Controls.Add(this.lblClick);
            this.Controls.Add(this.cbxDenominatios);
            this.Controls.Add(this.numericNew);
            this.Controls.Add(this.numericEdit);
            this.Controls.Add(this.dgDenominatios);
            this.Controls.Add(this.toolStrip1);
            this.Name = "RechargeATM";
            this.Text = "RechargeATM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RechargeATM_FormClosing);
            this.Load += new System.EventHandler(this.RechargeATM_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDenominatios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNew)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton cancel;
        private System.Windows.Forms.ToolStripButton delete;
        private System.Windows.Forms.DataGridView dgDenominatios;
        private System.Windows.Forms.NumericUpDown numericEdit;
        private System.Windows.Forms.NumericUpDown numericNew;
        private System.Windows.Forms.ComboBox cbxDenominatios;
        private System.Windows.Forms.Label lblClick;
    }
}