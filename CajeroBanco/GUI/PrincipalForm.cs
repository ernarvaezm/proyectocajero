﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CajeroBanco.GUI
{
    public partial class PrincipalForm : Form
    {
        public PrincipalForm()
        {
            InitializeComponent();
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            LoginUserForm userLo = new LoginUserForm();
            userLo.Show();
            this.Hide();

        }

        private void btnCostumer_Click(object sender, EventArgs e)
        {
            LoginCostumerForm costumerLo = new LoginCostumerForm();
            costumerLo.Show();
            this.Hide();
        }

        private void PrincipalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.ExitThread();
        }
    }
}
