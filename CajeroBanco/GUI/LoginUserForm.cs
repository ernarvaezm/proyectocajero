﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CajeroBanco.GUI
{
    public partial class LoginUserForm : Form
    {

        public int saActivado ;

        public LoginUserForm()
        {
            InitializeComponent();
            txtContrasenna.PasswordChar = '*';
        }

        private void LoginUserForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Boolean flag = false;
            String ruta =  @"..\..\Data\Empleados.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            foreach (XmlNode node in doc.SelectNodes("//employer"))
            {
                String userName = node.SelectSingleNode("userName").InnerText;
 
                String passwork = node.SelectSingleNode("passWord").InnerText;
                while ((userName == txtNombreUsuario.Text & passwork == txtContrasenna.Text))
                {
                    if (userName == "sa")
                    {
                        this.saActivado = 1;
                    }
                    flag = true;
                    this.OpenAdmin(flag);
                    return;
                }
            }
            this.OpenAdmin(flag);
        }


        private void OpenAdmin(Boolean flag)
        {
            if (flag)
            {
                PrincipalUserForm pr = new PrincipalUserForm(saActivado);
              
                pr.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Usuario o contraseña inválida");
                txtNombreUsuario.Text = String.Empty; txtContrasenna.Text = String.Empty;
            }
        }
    }
}
