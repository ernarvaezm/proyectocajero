﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using System.Xml;
using System.IO;

namespace CajeroBanco.GUI
{
    public partial class ATM_recharge : Form
    {
        //atributos
        private String FILE_NAME = @"C:\Users\Eliecer\Desktop\CajeroBancoSabado\CajeroBanco\Data\DenominationsTAM.xml";
         private DataTable dt;
         private DataTable Table = new DataTable(); //Declaramos una variable de tipo DataTable 
         private DataRow Renglon;//Esta variable de tipo DataRow 
         private  Dictionary<string, string> dictionary;
        public ATM_recharge()
        {
            InitializeComponent();
            // loadt();
            //Table.Columns.Add(new DataColumn("Denominacion"));
            //Table.Columns.Add(new DataColumn("Cantidad de billetes"));
            //numericEdit.Visible = false;
            //btnSave.Enabled = false;
            //btnCancel.Enabled = false;
            //dt = new DataTable();
            //dt.TableName = "bill";
            //dt.Columns.Add("name", typeof(String));
            //dt.Columns.Add("amount", typeof(String));
            //dt.Columns["type"].Caption = "tipo";
            //this.dg.DataSource = dt;// se pega al origen de datos
            //this.ReadXML();
            //this.dg.Columns[0].HeaderText = dt.Columns[0].Caption;
           
          
        }
        private void loadt()
        {   
            XmlDocument document = new XmlDocument();

          dictionary = new Dictionary<string, string>();

            String ruta = @"..\..\Data\AllDenominations.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            dictionary = new Dictionary<string, string>();
            foreach (XmlNode node in doc.SelectNodes("//bill"))
            {
                String type = node.SelectSingleNode("type").InnerText;
         
                dictionary.Add(type, type);
            }
            this.cbxDenominatios.DisplayMember = "Key";
            this.cbxDenominatios.ValueMember = "Value";
            this.cbxDenominatios.DataSource = new BindingSource(dictionary, null);
            this.cbxDenominatios.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }
        //private void LoadControls()
        //{
        //    DataGridViewRow row = this.dg.Rows[dg.CurrentRow.Index];
        //    if (row.Cells[0].Value != null)
        //    {
        //        this.txtnumeric.Value = Convert.ToDecimal (row.Cells["type"].Value.ToString());
        //    }
        //}

        private void ATM_recharge_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_1(object sender, EventArgs e)

        {
            if (dg.RowCount <= 5)
            {
                Renglon = Table.NewRow();
                string l = txtnumeric.Value.ToString();

                dictionary.Remove((string)cbxDenominatios.SelectedValue);

                Renglon[0] = (string)cbxDenominatios.SelectedValue.ToString();
                Renglon[1] = l;

                Table.Rows.Add(Renglon);

                nada();
                this.cbxDenominatios.DisplayMember = "Key";
                this.cbxDenominatios.ValueMember = "Value";
                this.cbxDenominatios.DataSource = new BindingSource(dictionary, null);
                this.cbxDenominatios.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            else
            {
                MessageBox.Show("Solo se permiten 5 denominaciones ");
            }
             
            
       
        }

    public void nada(){   

           dg.DataSource = Table;
       
    }

    private void button2_Click(object sender, EventArgs e)
    {

        try
        {
                 string message = "Seguro que quiere eliminar este cliente ?";
            const string caption = " Warning ";
            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                dg.Rows.Remove( dg.CurrentRow);
            }
        }
        catch (Exception)
        {

            MessageBox.Show("Seleccione algo");
        }

        }
       
        

    private void dataGridView1_Click(object sender, EventArgs e)
    {
        try
        {
            numericEdit.Visible = true;
            txtnumeric.Visible = false;
            cbxDenominatios.Visible = false;
            numericEdit.Value = int.Parse(dg.CurrentRow.Cells[1].Value.ToString());
            labelClik.Text = dg.CurrentRow.Cells[0].Value.ToString();
            btnAdd.Enabled = false;
            btnDelete.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            labelClik.Visible = true;
        }
        catch (Exception)
        {
            
            
        }
      
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
        btnDelete.Enabled = true;
        btnAdd.Enabled = true;
        btnCancel.Enabled = false;
        btnCancel.Enabled = false;
        btnSave.Enabled = false;
        txtnumeric.Visible = true;
        cbxDenominatios.Visible = true;
        numericEdit.Visible = false;
        labelClik.Visible = false;
    }

    private void btnSave_Click(object sender, EventArgs e)
    {

      //dg.CurrentRow.Cells[1].Value=numericEdit.Value;
    }

    private void btnSave_Click_1(object sender, EventArgs e)
    {

    }

   
           
        
    }
}
