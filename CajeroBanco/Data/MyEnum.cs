﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CajeroBanco.Data
{
    public enum TypeOfNumber 
    {
        /// <summary>
        /// numero entero
        /// </summary>
        Integer,
        /// <summary>
        /// numero con decimales
        /// </summary>
        Double
    }

    public enum ValidationCostrain
    {
         mustBeNumber,
        mustBeRequerid,
        mustBeAlphaNumeric,
        minumulLengeth,
        maximulLength
    }

    #region Buttons State Enums
    /// <summary>
    /// Estado del botón nuevo
    /// </summary>
    public enum NewButtonState
    {
        Enable,
        Disable
    }
  
    public enum SaveButtonState
    { 
        Enable,
        Disable
    }
    public enum CancelButtonState
    {
        Enable,
        Disable
    }
    public enum DeleteButtonState
    {
        Enable,
        Disable
    }
    public enum FindButtonState
    {
        Enable,
        Disable
    }
   
    public enum ExitButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Control State Enums
    public enum StateOfControls
    { 
        Enable,
        Disable
    }
    #endregion
    }

