﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CajeroBanco.Entity;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace CajeroBanco.DAO
{
    class UserDAO
    {
          //atributos
        private   User _user ;
        static XmlDocument document = new XmlDocument();
        static string ruta = @"..\..\Data\Empleados.xml";


        public UserDAO(User user)
	{
        this._user = user;
	}
       public UserDAO ()
	  {

    	}


        public void addUser() 
        {

         //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            XmlNode user = CreateNodeXml();

            //Obtenemos el nodo raiz del documento.
            XmlNode nodeRoot = document.DocumentElement;

          //Insertamos el nodo empleado al final del archivo
            nodeRoot.InsertAfter(user, nodeRoot.LastChild);
      
            document.Save(ruta);

        }
        /// <summary>
        ///  se encarga de crear un nuevo usuario 
        /// </summary>
        /// <returns> retorna el usuario </returns>

        private   XmlNode CreateNodeXml()
        {
            //Creamos el nodo que deseamos insertar.
            XmlElement user = document.CreateElement("employer");
            
            //Creamos el elemento idEmpleado.
            XmlElement idUser = document.CreateElement("idUser");
            idUser.InnerText =_user._identification ;
            user.AppendChild(idUser);

            //Creamos el elemento nombre.
            XmlElement name = document.CreateElement("name");
            name.InnerText = _user._name;
            user.AppendChild(name);

            //Creamos el elemento apellidos.
            XmlElement lastName = document.CreateElement("lastName");
            lastName.InnerText = _user._lastName;
            user.AppendChild(lastName);


            //Creamos el elemento user Name.
            XmlElement userName = document.CreateElement("userName");
            userName.InnerText = _user.UserName1;
            user.AppendChild(userName);


            //Creamos el elemento passWord.
            XmlElement passWord = document.CreateElement("passWord");
            passWord.InnerText = _user.PassWord;
            user.AppendChild(passWord);

          
            return user;
        }


        /// <summary>
        /// se encarga de cargar todos los usuarios del XML para mostrarlos en una tabla 
        /// </summary>
        /// <returns> una linkedList con todos los usuarios </returns>

        public LinkedList<User> loadEmployers()
        {
           
            //Creamos un documento y lo cargamos con los datos del XML.
            XmlDocument document = new XmlDocument();
            document.Load(ruta);
            //Obtenemos una colección con todos los empleados.
            XmlNodeList listEmployersNode = document.SelectNodes("empleados/employer");

            //Creamos un único empleado.
            XmlNode unEmployer;
             
            //creamos una lista de empleados
            	LinkedList<User> listEmployers = new LinkedList<User>();
            //Recorremos toda la lista de clientes
            for (int i = 0; i < listEmployersNode.Count; i++)
            {

                //Obtenemos cada empleado.
                unEmployer = listEmployersNode.Item(i);
                string id = unEmployer.SelectSingleNode("idUser").InnerText; 
              string name =unEmployer.SelectSingleNode("name").InnerText;
              string lastName = unEmployer.SelectSingleNode("lastName").InnerText;
             
            string pass = unEmployer.SelectSingleNode("passWord").InnerText; 
          
           string userName = unEmployer.SelectSingleNode("userName").InnerText;
                 User cm = new  User(id,name,lastName,userName,pass);
          
                 
              listEmployers.AddFirst(cm );
            }
            return listEmployers;
        }

        /// <summary>
        /// modifica los datos del empleado 
        /// </summary>
        /// <param name="id"> el id del usuario que va ser modificado </param>
        public void modifyXml(string id)
        {
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement empleados = document.DocumentElement;

            //Obtenemos la lista de todos los empleados.
            XmlNodeList listaEmpleados = document.SelectNodes("empleados/employer");


            foreach (XmlNode item in listaEmpleados)
            {
                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == id)
                {
                    Console.WriteLine(item.FirstChild.InnerText);
                    //Nodo sustituido.
                    XmlNode nodoOld = item;

                    //Nodo nuevo.
                    XmlNode nodoNew = CreateNodeXml();
                  
                    //Remplazamos el nodo.
                    empleados.ReplaceChild( nodoNew,nodoOld);
                }
            }
            //Salvamos el documento.
            document.Save(ruta);

        }

        /// <summary>
        /// elimina un empleado
        /// </summary>
        /// <param name="id"> recibe el id del empleado que se quiere eliminar  </param>
        public  void delete(string id)
        {
      
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);
               //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList ListCostumer = document.SelectNodes("empleados/employer");
       
            foreach (XmlNode item in ListCostumer)
            {
                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == id)
                {
                    Console.WriteLine(item.FirstChild.InnerText);
                    //Nodo sustituido.
                    XmlNode nodoOld = item;

                    //Borrar un nodo.
                   costumers.RemoveChild(nodoOld);
                }
            }
            //Salvamos el documento.
            document.Save(ruta);

        }

    }
}
