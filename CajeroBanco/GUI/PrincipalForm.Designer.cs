﻿namespace CajeroBanco.GUI
{
    partial class PrincipalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUser = new System.Windows.Forms.Button();
            this.btnCostumer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUser
            // 
            this.btnUser.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnUser.Image = global::CajeroBanco.Properties.Resources._1423277692_supportfemale_48;
            this.btnUser.Location = new System.Drawing.Point(30, 101);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(85, 47);
            this.btnUser.TabIndex = 1;
            this.btnUser.UseVisualStyleBackColor = true;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            // 
            // btnCostumer
            // 
            this.btnCostumer.Image = global::CajeroBanco.Properties.Resources._1423277765_malecostume_48;
            this.btnCostumer.Location = new System.Drawing.Point(188, 101);
            this.btnCostumer.Name = "btnCostumer";
            this.btnCostumer.Size = new System.Drawing.Size(81, 47);
            this.btnCostumer.TabIndex = 2;
            this.btnCostumer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCostumer.UseVisualStyleBackColor = true;
            this.btnCostumer.Click += new System.EventHandler(this.btnCostumer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selecciona una Opcion ";
            // 
            // PrincipalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCostumer);
            this.Controls.Add(this.btnUser);
            this.Name = "PrincipalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrincipalForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnCostumer;
        private System.Windows.Forms.Label label1;
    }
}