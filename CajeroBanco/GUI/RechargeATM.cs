﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using System.Xml;

namespace CajeroBanco.GUI
{
    public partial class RechargeATM : Form
    {
        private Boolean isEditing;   //Esta varibale dice si esta editando 
        private Dictionary<string, string> dictionary;
        private String FILE_NAME = @"C:\Users\Eliecer\Desktop\CajeroBancoSabado\CajeroBanco\Data\DenominatiosATM.xml";
          private DataTable dt;
         private DataTable Table = new DataTable(); //Declaramos una variable de tipo DataTable 
         private DataRow Renglon;//Esta variable de tipo DataRow 
        public RechargeATM()
        {
            InitializeComponent();
            dt = new DataTable();
            dt.TableName = "bill";
            dt.Columns.Add("name", typeof(String));
            dt.Columns.Add("amount", typeof(String));
            dt.Columns["name"].Caption = "Nombre";
            dt.Columns["amount"].Caption = "cantidad";
            this.dgDenominatios.DataSource = dt;// se pega al origen de datos
        
            this.dgDenominatios.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgDenominatios.Columns[1].HeaderText = dt.Columns[1].Caption;
            defaultButtons();
            loadt();
            this.ReadXML();
        }


        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        private void defaultButtons()
        {

            btnNew.Enabled = true;
            btnSave.Enabled = false;
            delete.Enabled = true;
            cancel.Enabled = false;
            numericEdit.Visible = false;

        }
        private void RechargeATM_Load(object sender, EventArgs e)
        {

        }

        private void loadt()
        {
            XmlDocument document = new XmlDocument();

            dictionary = new Dictionary<string, string>();

            String ruta = @"..\..\Data\AllDenominations.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            dictionary = new Dictionary<string, string>();
            foreach (XmlNode node in doc.SelectNodes("//bill"))
            {
                String type = node.SelectSingleNode("type").InnerText;

                dictionary.Add(type, type);
            }
            this.cbxDenominatios.DisplayMember = "Key";
            this.cbxDenominatios.ValueMember = "Value";
            this.cbxDenominatios.DataSource = new BindingSource(dictionary, null);
            this.cbxDenominatios.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        private void button1_Click(object sender, EventArgs e)
        {
           

        }
        private Boolean exist(string name)
        {

          
            String ruta =  @"..\..\Data\DenominatiosATM.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            foreach (XmlNode node in doc.SelectNodes("//bill"))
            {
                String Name = node.SelectSingleNode("name").InnerText;
 
                
                    if (name== Name)
                    {
                        return true;
                    }

                
                }
            return false;
        }

        private void Save_Click(object sender, EventArgs e)
        {



            if (isEditing)
            {
             this.dgDenominatios["name", dgDenominatios.CurrentRow.Index].Value = lblClick.Text;
            this.dgDenominatios["amount", dgDenominatios.CurrentRow.Index].Value = numericEdit.Value;

            }
            else
            {
                if (dgDenominatios.RowCount <= 5)
                {
                    if (exist((string)cbxDenominatios.SelectedValue))
                    {
                        MessageBox.Show("Denominacion existente");
                    }
                    else
                    {
                        DataRow dr;
                        dr = dt.NewRow();
                        dr["name"] = cbxDenominatios.SelectedValue;
                        dr["amount"] = numericNew.Value;

                        dt.Rows.Add(dr);
                    }
                   
                  

                    //dictionary.Remove((string)cbxDenominatios.SelectedValue);
               
                    //this.cbxDenominatios.DisplayMember = "Key";
                    //this.cbxDenominatios.ValueMember = "Value";
                    //this.cbxDenominatios.DataSource = new BindingSource(dictionary, null);
                    //this.cbxDenominatios.DropDownStyle = ComboBoxStyle.DropDownList;
                    //  nada();
                    //  ReadXML();

                }

            }
           
            WriteXML();
            defaultButtons();
          
           
            
        }

        private void newB_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            cancel.Enabled = true;
            btnNew.Enabled = false;
            isEditing = false;
            numericEdit.Visible = false;
            numericNew.Visible = true;
            cbxDenominatios.Visible = true;
        }

        private void cancel_Click(object sender, EventArgs e)
        {
           delete.Enabled = true;
           btnNew.Enabled = true;
           cancel.Enabled = false;

            btnSave.Enabled = false;
            numericNew.Visible = true;
            cbxDenominatios.Visible = true;
            numericEdit.Visible = false;
            lblClick.Visible = false;
        }

        public void nada()
        {

            dgDenominatios.DataSource = Table;

        }
        private void dgDenominatios_Click(object sender, EventArgs e)
        {
            try
            {
            btnNew.Enabled = false;
            cancel.Enabled = true;
            cbxDenominatios.Visible = false;
            numericNew.Visible = false;
            numericEdit.Visible = true;
            lblClick.Visible = true;
            numericEdit.Value = int.Parse(dgDenominatios.CurrentRow.Cells[1].Value.ToString());
            lblClick.Text = dgDenominatios.CurrentRow.Cells[0].Value.ToString();
            btnSave.Enabled = true;
            isEditing = true;

            }
            catch (Exception)
            {
                
                
            }
            
        }

        private void delete_Click(object sender, EventArgs e)
        {
            try
            {
dgDenominatios.Rows.RemoveAt(dgDenominatios.CurrentRow.Index);
                this.dgDenominatios.Refresh();

                if (dgDenominatios.Rows.Count <= 0)
                    defaultButtons();
                    this.WriteXML();
            }
            catch (Exception)
            {

                MessageBox.Show("Selecciones una fila");
            }
                

            
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           
            this.WriteXML();
            ReadXML();

        }

        private void RechargeATM_FormClosing(object sender, FormClosingEventArgs e)
        {
            PrincipalUserForm pu = new PrincipalUserForm(0);
            pu.Show();
        }   
    }
}
