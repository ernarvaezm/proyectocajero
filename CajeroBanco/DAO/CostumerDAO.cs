﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using CajeroBanco.DAO;
using CajeroBanco.Entity;
using System.Data;
using System.IO;


namespace CajeroBanco.DAO
{


    class CostumerDAO
    {
        //atributos
    
        private Costumer _newCostume;
        static XmlDocument document = new XmlDocument();
        static string ruta = @"..\..\Data\Clientes.xml";
        static string rutaBills = @"..\..\Data\AllDenominations.xml";

        public  CostumerDAO (Costumer newCostumer)
        {
            this._newCostume = newCostumer;

        }
        public CostumerDAO()
        {
        
        }

        public void addCostumer() 
        {

         //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            XmlNode costumer = CreateNodeXml();

            //Obtenemos el nodo raiz del documento.
            XmlNode nodeRoot = document.DocumentElement;


            //Insertamos el nodo empleado al final del archivo
            nodeRoot.InsertAfter(costumer, nodeRoot.LastChild);
              document.Save(ruta);

        }
         /// <summary>
         /// se  encarga de crear un nodo donde de tipo cliente
         /// </summary>
         /// <returns> un nodo con todos los datos del cliente</returns>
        private   XmlNode CreateNodeXml()
        {
            //Creamos el nodo que deseamos insertar.
            XmlElement costumer = document.CreateElement("costumer");
            
            //Creamos el elemento id usurio.
            XmlElement idCostumer = document.CreateElement("idCostumer");
            idCostumer.InnerText =_newCostume._identification ;
            costumer.AppendChild(idCostumer);

            //Creamos el elemento nombre.
            XmlElement name = document.CreateElement("name");
            name.InnerText = _newCostume._name;
            costumer.AppendChild(name);

            //Creamos el elemento apellidos.
            XmlElement lastName = document.CreateElement("lastName");
            lastName.InnerText = _newCostume._lastName;
            costumer.AppendChild(lastName);

            //Creamos el elemento accountNumber.
            XmlElement accountNumber = document.CreateElement("accountNumber");
            accountNumber.InnerText = _newCostume.AccountNumber;
            costumer.AppendChild(accountNumber);


            //Creamos el elemento passWord.
            XmlElement passWord = document.CreateElement("passWord");
            passWord.InnerText = _newCostume.Pass;
            costumer.AppendChild(passWord);

            //Creamos el elemento monto.
            XmlElement monto = document.CreateElement("monto");
            monto.InnerText = Convert.ToString(_newCostume.Money);
            costumer.AppendChild(monto);

       
            return costumer;
        }
       


        /// <summary>
        /// se encarga  de cargar todos los clientes
        /// </summary>
        /// <returns> una lsita con todos los clientes </returns>

        public LinkedList<Costumer> loadCostumers()
        {
            //Creamos un documento y lo cargamos con los datos del XML.
            XmlDocument document = new XmlDocument();
            document.Load(ruta);
            //Obtenemos una colección con todos los empleados.
            XmlNodeList listCostumersNode = document.SelectNodes("costumers/costumer");

            //Creamos un único cliente.
            XmlNode unCostumer;
             
            //creamos una lista de clientes
            	LinkedList<Costumer> Listcostumers = new LinkedList<Costumer>();
            //Recorremos toda la lista de clientes
            for (int i = 0; i < listCostumersNode.Count; i++)
            {

                //Obtenemos cada cliente.
                unCostumer = listCostumersNode.Item(i);
                string id = unCostumer.SelectSingleNode("idCostumer").InnerText; 
              string name =unCostumer.SelectSingleNode("name").InnerText;
              string lastName = unCostumer.SelectSingleNode("lastName").InnerText;
              string accountNumber = unCostumer.SelectSingleNode("accountNumber").InnerText;
              string pass = unCostumer.SelectSingleNode("passWord").InnerText;
              string mont = unCostumer.SelectSingleNode("monto").InnerText;   

               Costumer cm = new Costumer(id,name,lastName,accountNumber,pass,Convert.ToDouble(mont));

               Listcostumers.AddFirst(cm);


            }
            return Listcostumers;
        }
        /// <summary>
        /// se encarga de editar la informacion de un cliente
        /// </summary>
        /// <param name="id"> recibe el id del cliente a editar </param>
        public void editXml(string id)
        {
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList listCostumers = document.SelectNodes("costumers/costumer");


            foreach (XmlNode item in listCostumers)
            {
                //Determinamos el nodo a modificar por medio del id del cliente.
                if (item.FirstChild.InnerText == id)
                {
               
                    //Nodo sustituido.
                    XmlNode nodoOld = item;

                    //Nodo nuevo.
                    XmlNode nodoNew = CreateNodeXml();
                  
                    //Remplazamos el nodo.
                    costumers.ReplaceChild( nodoNew,nodoOld);
                }
            }
            //Salvamos el documento.
            document.Save(ruta);

        }

        /// <summary>
        ///  se encarga de eliminar un cliente
        /// </summary>
        /// <param name="id"> el id del cliente que se quiere eliminar </param>
        public  void delete(string id)
        {
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList ListCostumer = document.SelectNodes("costumers/costumer");


            foreach (XmlNode item in ListCostumer)
            {
                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == id)
                {
              
                    //Nodo sustituido.
                    XmlNode nodoOld = item;

                    //Borrar un nodo.
                   costumers.RemoveChild(nodoOld);
                }
            }
            //Salvamos el documento.
            document.Save(ruta);

        }

        public LinkedList<Costumer> find (string id)
        {

            LinkedList<Costumer> Listcostumers = new LinkedList<Costumer>();
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            XmlNode unCostumer;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList ListCostumer = document.SelectNodes("costumers/costumer");

            foreach (XmlNode item in ListCostumer)
            {
                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == id)
                {

                    //Obtenemos cada cliente.
                    unCostumer = item;
                    string ID = unCostumer.SelectSingleNode("idCostumer").InnerText;
                    string name = unCostumer.SelectSingleNode("name").InnerText;
                    string lastName = unCostumer.SelectSingleNode("lastName").InnerText;
                    string accountNumber = unCostumer.SelectSingleNode("accountNumber").InnerText;
                    string pass = unCostumer.SelectSingleNode("passWord").InnerText;
                    string mont = unCostumer.SelectSingleNode("monto").InnerText;

                    Costumer cm = new Costumer(id, name, lastName, accountNumber, pass, Convert.ToDouble(mont));

                    Listcostumers.AddFirst(cm);
 
                }
            }
            return Listcostumers;
        }

        public Costumer findC(string id)
        {
            Costumer cs = new Costumer();
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            XmlNode unCostumer;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList ListCostumer = document.SelectNodes("costumers/costumer");

            foreach (XmlNode item in ListCostumer)
            {
                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == id)
                {

                    //Obtenemos cada cliente.
                    unCostumer = item;
                    string ID = unCostumer.SelectSingleNode("idCostumer").InnerText;
                    string name = unCostumer.SelectSingleNode("name").InnerText;
                    string lastName = unCostumer.SelectSingleNode("lastName").InnerText;
                    string accountNumber = unCostumer.SelectSingleNode("accountNumber").InnerText;
                    string pass = unCostumer.SelectSingleNode("passWord").InnerText;
                    string mont = unCostumer.SelectSingleNode("monto").InnerText;

                    Costumer cm = new Costumer(id, name, lastName, accountNumber, pass, Convert.ToDouble(mont));

                    return cm;


                }
            }

            return cs;
        }

        
        public Boolean canTransfer(string account, string amount, string id)
        {
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList ListCostumer = document.SelectNodes("costumers/costumer");
            Costumer cs = new Costumer();
            Costumer df = new Costumer();

            this._newCostume = new Costumer();
            foreach (XmlNode item in ListCostumer)
            {

                //Determinamos el nodo a modificar por medio del id de empleado.
                if (item.FirstChild.InnerText == account)
                {
                    cs = findC(id);

                    if (cs.Money > double.Parse((amount)))
                    {
                        this._newCostume = cs;
                        this._newCostume.Money = cs.Money - double.Parse(amount);
                        editXml(id);


                        df = findC(account);
                        this._newCostume = df;
                        this._newCostume.Money = df.Money + double.Parse(amount);
                        editXml(account);



                        return true;
                    }

                }
            }

            return false;
        }
        /// <summary>
        /// metodo que cambia las contraseñas del cliente
        /// </summary>
        /// <param name="newPassword"> contraseña nueva</param>
        /// <param name="oldPassWord"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Boolean ChangePassword(string newPassword, string oldPassWord, string id)
        {
            Costumer cm;

            cm = findC(id);
            if (cm.Pass == oldPassWord)
            {
                _newCostume = cm;
                _newCostume.Pass = newPassword;
                editXml(id);

                return true;
            }
            return false;
        }
        /// <summary>
        /// metodo que retira dinero
        /// </summary>
        /// <param name="amount"> monto que va retirar </param>
        /// <param name="id">id del cliente</param>
        /// <returns></returns>
        public Boolean cashWithdrawal(string amount, string id)
        {
            Costumer cm;
            Costumer sc;
            cm = findC(id);
            if (cm.Money >= double.Parse(amount))
            {
                _newCostume = cm;
                _newCostume.Money = (cm.Money - double.Parse(amount));
                editXml(id);
                return true;
            }
            return false;
        }
        public string balance(string id)
        {
            return Convert.ToString(findC(id).Money);

        }
         

        public  Boolean restarBill(string pmonto)
        {

    
             denominations dm=new denominations();
     
    

            if (dm.restarBill(pmonto))
            {
               
                int[] numeros = new int[5];

                String ruta = @"..\..\Data\DenominatiosATM.xml";
                XmlDocument doc = new XmlDocument();
                doc.Load(ruta);
                int i = 0;
                foreach (XmlNode node in doc.SelectNodes("//bill"))
                {
                    String numer = node.SelectSingleNode("name").InnerText;
                    numeros[i] = int.Parse(numer);
                    i++;

                }
                Array.Sort(numeros);



                int monto = int.Parse(pmonto);

                while (monto > 0)
                {

                    if (monto == 0)
                    {
                        return true;
                    }
                    if (monto < numeros[0])
                    {
                        return false;
                    }

                    if (monto >= numeros[0] && monto < numeros[1])
                    {
                        //rebajar billete de 2 colones
                        monto = monto-numeros[0];
                        denominations dn = new denominations(numeros[0].ToString());
                        dn.deduct();
                    }
                    if (monto >= numeros[1] && monto < numeros[2])
                    {
                        //rebajar billete de 3 colones
                        monto = monto - numeros[1];
                        denominations dn = new denominations(numeros[1].ToString());
                        dn.deduct();
                    }
                    if (monto >= numeros[2] && monto < numeros[3])
                    {
                        //rebajar billete de 40 colones
                        monto = monto - numeros[2];
                        denominations dn = new denominations(numeros[2].ToString());
                        dn.deduct();
                    }
                    if (monto >= numeros[3] && monto < numeros[4])
                    {
                        //rebajar billete de 45 colones
                        monto = monto - numeros[3];
                        denominations dn = new denominations(numeros[3].ToString());
                        dn.deduct();
                    }
                    if (monto >= numeros[4])
                    {
                   
                        //rebajar billete de 434 colones
                        monto = monto - numeros[4];
                        denominations dn = new denominations(numeros[4].ToString());
                        dn.deduct();
                    }
                    if (monto == 0)
                    {
                        return true;
                    }

                }



            }
            else
            {
                MessageBox.Show("no hay billetas de esta denominacion ");
            }



            return false; 
        }
       
      
    }
}
