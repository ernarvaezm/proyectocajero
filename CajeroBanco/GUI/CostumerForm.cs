﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using CajeroBanco.Entity;
using CajeroBanco.Data;

namespace CajeroBanco.GUI
{
    public partial class CostumerForm : Form
    {
        //atributos
        private DataTable Table = new DataTable(); //Declaramos una variable de tipo DataTable 
        private DataRow Renglon;//Esta variable de tipo DataRow 
        private Boolean isEdit;   //Esta varibale dice si esta esditando 
        public CostumerForm()
        {

            InitializeComponent();
            //Le agregamos columnas a la variable Tabla que es de tipo DataTable
            Table.Columns.Add(new DataColumn("Cedula"));
            Table.Columns.Add(new DataColumn("Nombre"));
            Table.Columns.Add(new DataColumn("Apellido"));
            Table.Columns.Add(new DataColumn("Cuenta"));
            Table.Columns.Add(new DataColumn("Clave"));
            Table.Columns.Add(new DataColumn("Monto"));
            changeDate();
            textBoxPassWord.PasswordChar = '*';
        }

        private void CostumerForm_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// actualiza la tabla 
        /// </summary>
        public void changeDate()
        {
            LinkedList<Costumer> ListaClientes = new LinkedList<Costumer>();
            CostumerDAO cm = new CostumerDAO();


            ListaClientes = cm.loadCostumers();

            foreach (var item in ListaClientes)
            {
                Renglon = Table.NewRow();
                Costumer xn = item;
                Renglon[0] = xn._identification;
                Renglon[1] = xn._name;
                Renglon[2] = xn._lastName;
                Renglon[3] = xn.AccountNumber;
                Renglon[4] = xn.Pass;
                Renglon[5] = xn.Money;
                Table.Rows.Add(Renglon);

            }
            dataGridView1.DataSource = Table;


        }


        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {

                textBoxID.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString(); //Para obtener la cedula
                textBoxName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString(); //Para obtener el nombre
                textBoxLastname.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString(); //Para obtener el apellido 
                textBoxMoney.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString(); //Para obtener el monto
                textBoxPassWord.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString(); //Para obtener la contraseña
                textBoxAccountNumber.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                isEdit = true;
            }
            catch (Exception)
            {
           
            }

        }

        /// <summary>
        /// se encarga de limpiar todos los textbox
        /// </summary>


        public void ClearAll()
        {

            textBoxID.Text = string.Empty;
            textBoxName.Text = string.Empty;
            textBoxLastname.Text = string.Empty;
            textBoxMoney.Text = string.Empty;
            textBoxPassWord.Text = string.Empty;
            textBoxAccountNumber.Text = string.Empty;

        }
        /// <summary>
        /// se encarga de poner los los botones por defecto 
        /// </summary>

        private void defaultButtons()
        {

            newB.Enabled = true;
            Save.Enabled = false;
            delete.Enabled = true;
            find.Enabled = true;
            cancel.Enabled = false;

        }
        //boton de agregar un cliente nuevo
        private void nuevo_Click(object sender, EventArgs e)
        {
            cancel.Enabled = true;
            Save.Enabled = true;
            newB.Enabled = false;
            delete.Enabled = false;
            find.Enabled = false;
            ClearAll();
            isEdit = false;



        }
        //boton de eliminar
        private void delete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0 && dataGridView1.SelectedRows.Count > 0)
            {
                string message = "Seguro que quiere eliminar este cliente ?";
                const string caption = " Warning ";
                var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    CostumerDAO cm = new CostumerDAO();
                    cm.delete(dataGridView1.CurrentRow.Cells[0].Value.ToString()); ClearAll();
                    defaultButtons();
                    isEdit = false;
                    clearTable();
                    updateNew();
                }

            }
            else
                MessageBox.Show("Seleccione la fila que desea Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void cancel_Click(object sender, EventArgs e)
        {
            isEdit = false;
            defaultButtons();
            ClearAll();
        }

        private void Save_Click(object sender, EventArgs e)
        {

            if (isControlsOnValidState())
            {
         Costumer newCostumer = new Costumer(textBoxID.Text,
                            textBoxName.Text, textBoxLastname.Text,
                           textBoxAccountNumber.Text,
                            textBoxPassWord.Text, Convert.ToDouble(textBoxMoney.Text));
                CostumerDAO csm = new CostumerDAO(newCostumer);
                if (isEdit == true)
                {
                csm.editXml(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    isEdit = false;
                    MessageBox.Show("Modificado Correctamente", "Correcto", MessageBoxButtons.OK);
                }
                else
                {
                    csm.addCostumer();
                    MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK);
                 

                }
                defaultButtons();
                ClearAll();
                clearTable();
                updateNew();


            }
            else
            {

                this.textBoxName.Focus();

            }

            }

        private bool isControlsOnValidState()
        {
            Validator valName = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valDocumet = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valLastname = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valAccountNumber = new Validator(ValidationCostrain.mustBeRequerid ,ValidationCostrain.minumulLengeth,ValidationCostrain.minumulLengeth);
            Validator valPassword = new Validator(ValidationCostrain.mustBeRequerid,ValidationCostrain.minumulLengeth,ValidationCostrain.maximulLength);
            Validator valAmount = new Validator(ValidationCostrain.mustBeRequerid,ValidationCostrain.minumulLengeth,ValidationCostrain.maximulLength); 

            if (valDocumet.Validate("Identificacion", textBoxID.Text,20, 9) 
                & valName.Validate("Nombre", textBoxName.Text)
                & valLastname.Validate("Apellido",textBoxLastname.Text)  
                & valAccountNumber.Validate("Cuenta",textBoxAccountNumber.Text,9,1)
                & valPassword.Validate("Contraseña", textBoxAccountNumber.Text,15, 1)
             & valAmount.Validate("Monto", textBoxAccountNumber.Text,300 )
                )
            {
                return true;
            }
            else
            {
                MessageBox.Show(valDocumet.errorDescription 
                    + valName.errorDescription
                    +valLastname.errorDescription
                    +valAccountNumber.errorDescription 
                    +valPassword.errorDescription
                    +valAmount.errorDescription
                , "Eror al validar campos",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }

        public void clearTable()
        {
            Table.Clear();
            dataGridView1.DataSource = null;
        }

        private void updateNew()
        {
            changeDate();
            dataGridView1.Refresh();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

      
    }
}

