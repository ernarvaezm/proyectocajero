﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CajeroBanco.Data
{
    class Validator
    {
        public Boolean _isValidate { get; set; }

        public string errorDescription { get; set; }

        private Boolean _mustBeNumber;
        private Boolean _mustBeRequerid;
        private Boolean _mustBeAlphaNumeric;

        private Int32 ? _minumulLengeth;
        private Int32 ? _maximulLength;

        /// <summary>
        /// agrega la rectriccion recibida por parametro 
        /// 
        /// </summary>
        /// <param name="vc1"></param>
        public Validator(ValidationCostrain vc1)
        {
            addValidationConstraint(vc1);

        }
        public Validator(ValidationCostrain vc1, int minimumLength)
        {
            addValidationConstraint(vc1);
            this._minumulLengeth = minimumLength;


        }

        public Validator(ValidationCostrain vc1, int minimumLength, int maximuMLength)
        {
            addValidationConstraint(vc1);
            this._minumulLengeth = minimumLength;
            this._maximulLength = minimumLength;

        }



        public Validator(ValidationCostrain vc1, ValidationCostrain vc2)
        {
            addValidationConstraint(vc1);
            addValidationConstraint(vc2);

        }
        public Validator(ValidationCostrain vc1, int minimumLength, ValidationCostrain vc2)
        {
            addValidationConstraint(vc1);
            addValidationConstraint(vc2);

            this._minumulLengeth = minimumLength;


        }

        public Validator(ValidationCostrain vc1, int minimumLength, int maximuMLength, ValidationCostrain vc2)
        {
            addValidationConstraint(vc1);
            this._minumulLengeth = minimumLength;
            this._maximulLength = minimumLength;


        }


        public Validator(ValidationCostrain vc1, ValidationCostrain vc2, ValidationCostrain vc3)
        {
            addValidationConstraint(vc1);
            addValidationConstraint(vc2);
            addValidationConstraint(vc3);


        }
        public Validator(ValidationCostrain vc1, int minimumLength, ValidationCostrain vc2, ValidationCostrain vc3)
        {
            addValidationConstraint(vc1);
            addValidationConstraint(vc2);
            addValidationConstraint(vc3);
            this._minumulLengeth = minimumLength;


        }

        public Validator(ValidationCostrain vc1, int minimumLength, int maximuMLength, ValidationCostrain vc2, ValidationCostrain vc3)
        {
            addValidationConstraint(vc1);
            this._minumulLengeth = minimumLength;
            this._maximulLength = minimumLength;
            addValidationConstraint(vc3);

        }
        private void addValidationConstraint(ValidationCostrain validationConstraint)
        {
            switch (validationConstraint)
            {
                case ValidationCostrain.mustBeNumber:
                    this._mustBeNumber = true;
                    break;
                case ValidationCostrain.mustBeRequerid:
                    this._mustBeRequerid = true;
                    break;
                case ValidationCostrain.mustBeAlphaNumeric:
                    this._mustBeAlphaNumeric = true;
                    break;


            }
        }

        public Boolean Validate(string key, string value, int minimun)
        {
            this._minumulLengeth = minimun;
            return Validate(key, value);

        }
        public Boolean Validate(string key, string value, int maximun, int minimun)
        {

            this._minumulLengeth = minimun;
            this._maximulLength = maximun;
            return Validate(key, value);

        }

        public Boolean Validate(string key, string value)
        {
            errorDescription = string.Empty;
            _isValidate = true;
            string localErrorMessage = string.Empty;
            if (_mustBeRequerid)
            {
                if (value == string.Empty)
                {
                    this.errorDescription += "-Es un valor Requerido. \n";
                    this._isValidate = false;


                }
            }

            //validar que el valor sea alfanumerico

            if (_mustBeAlphaNumeric)
            {
                Regex alphanumericPatter = new Regex(" ^[a-zA-Z0-9]+$");
                if (!alphanumericPatter.IsMatch(value))
                {
                    this.errorDescription += "-Debe ser un valor alfanumerico . \n";
                    _isValidate = false;

                }
            }

            //validar que sea numero
            if (_mustBeNumber)
            {
                try
                {
                    localErrorMessage = string.Empty;
                    int i = int.Parse(value);
                }
                catch (Exception)
                {
                    this.errorDescription += "-debe ser un valor numerico .\n";
                    _isValidate = false;

                }

            }

            //validar el minimo 
            if (_minumulLengeth != null)
            {
                if (value.Length < _minumulLengeth)
                {
                    this.errorDescription += "- La longitud miminma es "
                        + _minumulLengeth + ".\n";

                }


            }
            //validar el maximo 

            if (_maximulLength !=null){
                if (value.Length > _maximulLength)
                {
                    this.errorDescription += "- La longitud maxima es "
                        + _maximulLength + ".\n";

                }
            }


            if (!_isValidate)

                this.errorDescription = "\nPara el campo \"" + key + " \" : \n "
                + this.errorDescription;

            return _isValidate;

        }
    }
}
