﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace CajeroBanco.Entity
{
    class denominations
    {
        private  string _name;
        private int _amount=0;
        static XmlDocument document = new XmlDocument();
        static string ruta = @"..\..\Data\DenominatiosATM.xml";

        public denominations(string name )
        {
           
            this._name = name;
          
        }
        public denominations()
        {

        }
        private void findAmount()
        { 
                XmlDocument doc = new XmlDocument();
                doc.Load(ruta);
        
                foreach (XmlNode node in doc.SelectNodes("//bill"))
                {
                     String numer = node.SelectSingleNode("name").InnerText;
                    if(_name==numer){
                         String amoun = node.SelectSingleNode("amount").InnerText;
                        
                         _amount =(Int32.Parse(amoun)-1);
                         MessageBox.Show("Tengo denominaciones " + _amount);
                    }

                }
        }
        
        public void deduct()
        {

           

           findAmount();
        
        

          //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);

            //Obtenemos el nodo raiz del documento.
            XmlElement costumers = document.DocumentElement;

            //Obtenemos la lista de todos los clientes.
            XmlNodeList listBill = document.SelectNodes("DocumentElement/bill");

           
            foreach (XmlNode item in listBill)
            {
                //Determinamos el nodo a modificar por medio del id del cliente.
                if (item.FirstChild.InnerText == _name)
                {
                   
                    //Nodo sustituido.
                    XmlNode nodoOld = item;

                    //Nodo nuevo.
                    XmlNode nodoNew = CreateNodeXml();
                  
                    //Remplazamos el nodo.
                    costumers.ReplaceChild( nodoNew,nodoOld);
                 
                }
            }
            //Salvamos el documento.
            document.Save(ruta);

        }
        private XmlNode CreateNodeXml()
        {
            //Creamos el nodo que deseamos insertar.
            XmlElement bill = document.CreateElement("bill");

            //Creamos el elemento id usurio.
            XmlElement name = document.CreateElement("name");
            name.InnerText = _name;
            bill.AppendChild(name);

            //Creamos el elemento nombre.
            XmlElement amount = document.CreateElement("amount");
            amount.InnerText = Convert.ToString (_amount);
            bill.AppendChild(amount);
          
            return  bill;
        }

        public Boolean restarBill(string pmonto)
        {  
            int[] numeros = new int[5];

            String ruta = @"..\..\Data\DenominatiosATM.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            int i = 0;
            foreach (XmlNode node in doc.SelectNodes("//bill"))
            {
                String numer = node.SelectSingleNode("name").InnerText;
                numeros[i] = int.Parse(numer);
                i++;

            }
            Array.Sort(numeros);



            int monto = int.Parse(pmonto);

            while (monto > 0)
            {

                if (monto == 0)
                {
                    return true;
                }
                if (monto < numeros[0])
                {
                    return false;
                }

                if (monto >= numeros[0] && monto < numeros[1])
                {
                    //rebajar billete de 2000 colones
                    monto = monto - numeros[0];


                }
                if (monto >= numeros[1] && monto < numeros[2])
                {
                    //rebajar billete de 3 colones
                    monto = monto - numeros[1];
                }
                if (monto >= numeros[2] && monto < numeros[3])
                {
                    //rebajar billete de 40 colones
                    monto = monto - numeros[2];
                }
                if (monto >= numeros[3] && monto < numeros[4])
                {
                    //rebajar billete de 45 colones
                    monto = monto - numeros[3];
                }
                if (monto >= numeros[4])
                {
                    //rebajar billete de 434 colones
                    monto = monto - numeros[4];
                }
                if (monto == 0)
                {
                    return true;
                }

            }

            return false;
        }
       
      

    }
}
