﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CajeroBanco.DAO;

namespace CajeroBanco.Entity 
{
    class User :Person
    {
        //atributos
        private string _UserName;
        private string _passWord;

        public User()
        {

        }

        public User(string id,string name ,string lastname,string userName, string passWord):
            base (id,name,lastname)
        {
            this._UserName = userName;
            this._passWord = passWord;
        }
        public string UserName1
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
    

        public string PassWord
        {
            get { return _passWord; }
            set { _passWord = value; }
        }

        public override void changePass()
        {
        }
    }
}
