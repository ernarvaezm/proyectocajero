﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CajeroBanco.DAO
{
    abstract  class Person
    {
        //atributos 
      public string _identification;
   public  string _lastName;
   public string _name;


        public Person()
        {

        }
        public Person(string id,string name,string lastName)
        {
            this._identification = id;
            this._lastName = lastName;
            this._name = name;


        }


        protected string Identification
        {
            get { return _identification; }
            set { _identification = value; }
        }
  

        protected string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        

        protected string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

      public abstract void  changePass();


    }
}
