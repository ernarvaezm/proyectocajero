﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CajeroBanco.GUI
{
    public partial class LoginCostumerForm : Form
    {
        private string idCostumer;
        public LoginCostumerForm()
        {
            InitializeComponent();
            txtPin.PasswordChar = '*';
        }

        private void LoginCostumerForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Boolean flag = false;
            String ruta = @"..\..\Data\Clientes.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            foreach (XmlNode node in doc.SelectNodes("//costumer"))
            {
                String userName = node.SelectSingleNode("accountNumber").InnerText;
                String passwork = node.SelectSingleNode("passWord").InnerText;
                while ((userName == txtCardNumber.Text & passwork == txtPin.Text))
                {
                    idCostumer=node.SelectSingleNode("idCostumer").InnerText;
                    flag = true;
                    this.OpenAdmin(flag);
                    return;
                }
            }
            this.OpenAdmin(flag);
        }

        private void OpenAdmin(Boolean flag)
        {
            if (flag)
            {
                PrincipalCostumerForm PCF = new PrincipalCostumerForm(idCostumer);
        
                PCF.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o contraseña inválida");
                txtCardNumber.Text = String.Empty; txtPin.Text = String.Empty;
            }
        }
    }
}
