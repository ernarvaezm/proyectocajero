﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CajeroBanco.GUI
{
    public partial class PrincipalUserForm : Form
    {  
        //atributos
        private CostumerForm CostumerForm;
    
        private UserForm userForm;
      public int _isSa ;


        public PrincipalUserForm( )
        {
            InitializeComponent();
            LoginUserForm lu = new LoginUserForm();
            _isSa = lu.saActivado;
            IsMdiContainer = true;


            if (_isSa==1)
            {
                miFuncionario.Visible = true;
            }
            else
            {
                miFuncionario.Visible = false;       
            }
   
        }
        public PrincipalUserForm(int x)
        {
            InitializeComponent();
        }


        public int IsSa
        {
            get { return _isSa; }
            set { _isSa = value; }
        }

        

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void PrincipalUserForm_Load(object sender, EventArgs e)
        {

        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try {
                if (CostumerForm.IsDisposed == true)
                    CostumerForm = new CostumerForm();
                else CostumerForm.Activate();
            }

            catch (Exception) {
                CostumerForm = new CostumerForm();
            }
            CostumerForm.MdiParent = this;
            CostumerForm.ShowInTaskbar = true;
            CostumerForm.StartPosition = FormStartPosition.CenterParent;
            CostumerForm.Show();
        }

        private void editarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RechargeATM reATM = new RechargeATM();
            reATM.Show();
            this.Hide();

        }

        private void denominacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (userForm.IsDisposed == true)
                    userForm = new UserForm();
                else userForm.Activate();
            }

            catch (Exception)
            {
                userForm = new UserForm();
            }
            userForm.MdiParent = this;
            userForm.ShowInTaskbar = true;
            userForm.StartPosition = FormStartPosition.CenterParent;
            userForm.Show();

        }

        private void PrincipalUserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            PrincipalForm pf = new PrincipalForm();
            pf.Show();
        }

        private void denominacionesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ATM_maintenance atm = new ATM_maintenance();
            atm.Show();
            this.Hide();
        }
    }
}
