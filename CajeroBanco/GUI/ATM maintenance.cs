﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using System.IO;
using System.Xml;


namespace CajeroBanco.GUI
{
    public partial class ATM_maintenance : Form
    {
        private Boolean isEditing;   //Esta varibale dice si esta editando 

  private String FILE_NAME = @"C:\Users\Eliecer\Desktop\CajeroBancoSabado\CajeroBanco\Data\AllDenominations.xml";
        
        private DataTable dt;
        public ATM_maintenance()
        {
            InitializeComponent();
            dt = new DataTable();
            dt.TableName = "bill";
            dt.Columns.Add("type", typeof(String));
            dt.Columns["type"].Caption = "tipo";
           this.dgDenominatios.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgDenominatios.Columns[0].HeaderText = dt.Columns[0].Caption;
            defaultButtons();
        }

        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

          private void LoadControls()
        {
            DataGridViewRow row = this.dgDenominatios.Rows[dgDenominatios.CurrentRow.Index];
            if (row.Cells[0].Value != null)
            {
                this.txtDeno.Text = row.Cells["type"].Value.ToString();
            }
        }
     
     
     
        private void defaultButtons()
        {
            newB.Enabled = true;
            Save.Enabled = false;
            delete.Enabled = true;
            cancel.Enabled = false;

        }
   

        private void Save_Click(object sender, EventArgs e)
        {
            if (txtDeno.Text!=string.Empty)
            {
                if (exist(txtDeno.Text))
                {
                    MessageBox.Show("Ya existe esta denominacion");
                }
                else
                {
                    if (isEditing)
                    {
                        this.dgDenominatios["type", dgDenominatios.CurrentRow.Index].Value = this.txtDeno.Text;
                    }
                    else
                    {

                        DataRow dr;
                        dr = dt.NewRow();
                        dr["type"] = txtDeno.Text;
                        dt.Rows.Add(dr);
                    }

                    this.isEditing = false;
                    this.ClearControls();
                    this.WriteXML();
             
           
                
            }
          

            }

         
            
        }
          private void ClearControls()
        {
            this.txtDeno.Text = string.Empty;
    
            this.txtDeno.Focus();
        }
        private void ATM_maintenance_Load(object sender, EventArgs e)
        {
               ClearControls();
            dgDenominatios.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgDenominatios.MultiSelect = false;
        }

        private void newB_Click(object sender, EventArgs e)
        {
            newB.Enabled = false;
            delete.Enabled = false;
 
            isEditing = false;
            cancel.Enabled = true;
            Save.Enabled = true;

        }

        private void cancel_Click(object sender, EventArgs e)
        {

            isEditing = false;
            defaultButtons();
        }

        private void dataDenominatios_Click(object sender, EventArgs e)
        {
            try
            {
                newB.Enabled = false;
                delete.Enabled = true;
             
                Save.Enabled = true;
                cancel.Enabled = false;

                txtDeno.Text = dgDenominatios.CurrentRow.Cells[0].Value.ToString(); //Para obtener el numero
             
                isEditing = true;
                cancel.Enabled = true;
            }
            catch (Exception)
            {


            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
             if (dgDenominatios.Rows.Count > 0 && dgDenominatios.SelectedRows.Count > 0)
            {
                dgDenominatios.Rows.RemoveAt(dgDenominatios.CurrentRow.Index);
                this.dgDenominatios.Refresh();
           
                if (dgDenominatios.Rows.Count <= 0)
                    ClearControls();
                    this.WriteXML();

            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        public Boolean exist (string id)
        {
         
            String ruta = @"..\..\Data\AllDenominations.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            foreach (XmlNode node in doc.SelectNodes("//bill"))
            {
                String type = node.SelectSingleNode("type").InnerText;
       
                while ((type == id))
                {

                    return true;
                }
            }

            return false;
        }

        private void ATM_maintenance_FormClosing(object sender, FormClosingEventArgs e)
        {
            PrincipalUserForm pu = new PrincipalUserForm(0);
            pu.Show();
        }

       
        
       
    }
}
