﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CajeroBanco.DAO
{ 
    class Denominations
    {

        static XmlDocument document = new XmlDocument();
        static string ruta = @"..\..\Data\Denominations.xml";

        public Denominations()
        {

        }

        public void denominations() { 
        
        }

        private XmlNode CreateNodeXml(string twoThousand, string fiveThousand, string tenThousand, string twentyThousand, string fiftyThousand)
        {
            //Creamos el nodo que deseamos insertar.
            XmlElement bills = document.CreateElement("bills");

            XmlElement TwoThousand = document.CreateElement("twoThousand");
            TwoThousand.InnerText = twoThousand;
            bills.AppendChild(TwoThousand);

            XmlElement FiveThousand = document.CreateElement("fiveThousand");
            FiveThousand.InnerText = fiveThousand;
            bills.AppendChild(FiveThousand);

            XmlElement TenThousand = document.CreateElement("tenThousand");
            TenThousand.InnerText = tenThousand;
            bills.AppendChild(TenThousand);


            XmlElement TwentyThousand = document.CreateElement("twentyThousand");
            TwentyThousand.InnerText = twentyThousand;
            bills.AppendChild(TwentyThousand);



            XmlElement FiftyThousand = document.CreateElement("fiftyThousand");
            FiftyThousand.InnerText = fiftyThousand;
            bills.AppendChild(FiftyThousand);
            return bills;
        }


        public void actualiz(string twoThousand, string fiveThousand, string tenThousand, string twentyThousand, string fiftyThousand)
        {
            //Cargamos el documento XML.
            document = new XmlDocument();
            document.Load(ruta);
    XmlElement bills = document.DocumentElement;
            XmlNodeList List = document.SelectNodes("Denominations/bills");
            foreach (XmlNode item in List)
            {
                //Nodo sustituido.
                XmlNode nodoOld = item;

                //Nodo nuevo.
                XmlNode nodoNew = CreateNodeXml( twoThousand,  fiveThousand,  tenThousand,  twentyThousand,  fiftyThousand);

                //Remplazamos el nodo.
             bills.ReplaceChild(nodoNew, nodoOld);
            }
            document.Save(ruta);

        }

    }
}
