﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using CajeroBanco.Entity;
using CajeroBanco.Data;

namespace CajeroBanco.GUI
{
    public partial class UserForm : Form
    {

        //atributos
        private DataTable Table = new DataTable(); //Declaramos una variable de tipo DataTable 
        private DataRow Renglon;//Esta variable de tipo DataRow 
        private Boolean isEditing;   //Esta varibale dice si esta esditando 
        public UserForm()
        {

            InitializeComponent();
            textBoxPass.PasswordChar = '*';
 
            Table.Columns.Add(new DataColumn("Cedula"));
            Table.Columns.Add(new DataColumn("Nombre"));
            Table.Columns.Add(new DataColumn("Apellido"));
            Table.Columns.Add(new DataColumn("Usuario"));
            Table.Columns.Add(new DataColumn("Contraseña"));
            defaultButtons();
            changeDate();
            

        }


        public void ClearAll()
        {

            textBoxID.Text = string.Empty;
            textBoxName.Text = string.Empty;
            textBoxLastname.Text = string.Empty;
            textBoxUser.Text = string.Empty;
            textBoxPass.Text = string.Empty;
        }

        private void defaultButtons()
        {

            newB.Enabled = true;
            Save.Enabled = false;
            delete.Enabled = true;
            find.Enabled = true;
            cancel.Enabled = false;

        }
        /// <summary>
        /// actualiza la tabla 
        /// </summary>
        public void changeDate()
        {

            LinkedList<User> ListEmployers = new LinkedList<User>();
            UserDAO cm = new UserDAO();
            ListEmployers = cm.loadEmployers();

            foreach (var item in ListEmployers)
            {
                Renglon = Table.NewRow();
                User xn = item;
                Renglon[0] = xn._identification;
                Renglon[1] = xn._name;
                Renglon[2] = xn._lastName;
                Renglon[3] = xn.UserName1;
                Renglon[4] = xn.PassWord;

                Table.Rows.Add(Renglon);
            }
            dataGridView1.DataSource = Table;
            this.dataGridView1.Columns[4].Visible = false;
        }




        private void newB_Click(object sender, EventArgs e)
        {

            newB.Enabled = false;
            delete.Enabled = false;
            find.Enabled = false;
            isEditing = false;
            cancel.Enabled = true;
            Save.Enabled = true;

        }
        private  Boolean isControlsOnValidState()
        {
            Validator valName = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valDocumet = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valLastname = new Validator(ValidationCostrain.mustBeRequerid);
            Validator valUserName = new Validator(ValidationCostrain.mustBeRequerid );
            Validator valPassword = new Validator(ValidationCostrain.mustBeRequerid, ValidationCostrain.minumulLengeth, ValidationCostrain.maximulLength);
          
            if (valDocumet.Validate("Identificacion", textBoxID.Text, 20, 9)
             & valName.Validate("Nombre", textBoxName.Text)
             & valLastname.Validate("Apellido", textBoxLastname.Text)
             & valUserName.Validate("Usuario", textBoxUser.Text)
             & valPassword.Validate("Contraseña", textBoxPass.Text, 15, 1)
             )
            {
                return true;
            }
            else
            {
                MessageBox.Show(valDocumet.errorDescription
                    + valName.errorDescription
                    + valLastname.errorDescription
                    + valUserName.errorDescription
                    + valPassword.errorDescription
                , "Eror al validar campos",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
      
        }
     

        private void Save_Click(object sender, EventArgs e)
        {
            //if (txtCode.Text)
            //{
                
            //}

            if (isControlsOnValidState())
            {
                User newUser = new User(textBoxID.Text, textBoxName.Text, textBoxLastname.Text, textBoxUser.Text, textBoxPass.Text);
                UserDAO csm = new UserDAO(newUser);
                if (isEditing == true)
                {
                    if (isControlsOnValidState())
                    {
                         csm.modifyXml(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    isEditing = false;
                    MessageBox.Show("Modificado Correctamente ", "Correcto", MessageBoxButtons.OK);
                    }
                   
                }
                else
                {

                    csm.addUser();
                    MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK);


                }
                defaultButtons();
                ClearAll();
                clearTable();
                updateNew();
            }
            else
            {
                this.textBoxName.Focus();
            }


        }


        private void delete_Click(object sender, EventArgs e)
        {

       

            if (dataGridView1.Rows.Count > 0 &&  dataGridView1.Rows.Count > 0 )
            {
                const string message =
          "Seguro que quiere eliminar este Usuario ?";
                const string caption = " Warning ";
                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    UserDAO useD = new UserDAO(); ;
                    useD.delete(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    ClearAll();
                    isEditing = false;
                    clearTable();
                    updateNew();
                }
            }
            else
            {
                MessageBox.Show("Seleccione la fila que desea Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }




        private void cancel_Click(object sender, EventArgs e)
        {
            isEditing = false;
            defaultButtons();
            ClearAll();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {

        }
        private void clearTable()
        {

            Table.Clear();
            dataGridView1.DataSource = null;
        }

        private void updateNew()
        {
            changeDate();
            dataGridView1.Refresh();
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                newB.Enabled = false;
                delete.Enabled = true;
                find.Enabled = false;
                Save.Enabled = true;
                cancel.Enabled = false;

                textBoxID.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString(); //Para obtener la cedula
                textBoxName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString(); //Para obtener el nombre
                textBoxLastname.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString(); //Para obtener el apellido 
                textBoxPass.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString(); //Para obtener el la contraseña
                textBoxUser.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString(); //Para obtener el usaurio
                isEditing = true;
            }
            catch (Exception)
            {
                
            
            }

     

        }

    }
}
