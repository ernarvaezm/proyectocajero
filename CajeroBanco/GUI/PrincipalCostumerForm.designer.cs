﻿namespace CajeroBanco.GUI
{
    partial class PrincipalCostumerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnchangePin = new System.Windows.Forms.Button();
            this.btnbalanceInquiry = new System.Windows.Forms.Button();
            this.btntransfer = new System.Windows.Forms.Button();
            this.btnCashWithdrawal = new System.Windows.Forms.Button();
            this.gbScream = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbScream.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnchangePin
            // 
            this.btnchangePin.Location = new System.Drawing.Point(11, 108);
            this.btnchangePin.Name = "btnchangePin";
            this.btnchangePin.Size = new System.Drawing.Size(121, 23);
            this.btnchangePin.TabIndex = 0;
            this.btnchangePin.Text = "Cambio de PIN";
            this.btnchangePin.UseVisualStyleBackColor = true;
            this.btnchangePin.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnbalanceInquiry
            // 
            this.btnbalanceInquiry.Location = new System.Drawing.Point(369, 99);
            this.btnbalanceInquiry.Name = "btnbalanceInquiry";
            this.btnbalanceInquiry.Size = new System.Drawing.Size(110, 23);
            this.btnbalanceInquiry.TabIndex = 1;
            this.btnbalanceInquiry.Text = "Consulta de saldo";
            this.btnbalanceInquiry.UseVisualStyleBackColor = true;
            this.btnbalanceInquiry.Click += new System.EventHandler(this.btnbalanceInquiry_Click);
            // 
            // btntransfer
            // 
            this.btntransfer.Location = new System.Drawing.Point(12, 59);
            this.btntransfer.Name = "btntransfer";
            this.btntransfer.Size = new System.Drawing.Size(121, 23);
            this.btntransfer.TabIndex = 2;
            this.btntransfer.Text = "Transferencia  fondos";
            this.btntransfer.UseVisualStyleBackColor = true;
            this.btntransfer.Click += new System.EventHandler(this.btntransfer_Click);
            // 
            // btnCashWithdrawal
            // 
            this.btnCashWithdrawal.Location = new System.Drawing.Point(369, 59);
            this.btnCashWithdrawal.Name = "btnCashWithdrawal";
            this.btnCashWithdrawal.Size = new System.Drawing.Size(110, 23);
            this.btnCashWithdrawal.TabIndex = 3;
            this.btnCashWithdrawal.Text = "Retiro Efectivo";
            this.btnCashWithdrawal.UseVisualStyleBackColor = true;
            this.btnCashWithdrawal.Click += new System.EventHandler(this.btnCashWithdrawal_Click);
            // 
            // gbScream
            // 
            this.gbScream.Controls.Add(this.textBox3);
            this.gbScream.Controls.Add(this.textBox2);
            this.gbScream.Controls.Add(this.textBox1);
            this.gbScream.Controls.Add(this.label3);
            this.gbScream.Controls.Add(this.label2);
            this.gbScream.Controls.Add(this.label1);
            this.gbScream.Location = new System.Drawing.Point(138, 47);
            this.gbScream.Name = "gbScream";
            this.gbScream.Size = new System.Drawing.Size(225, 131);
            this.gbScream.TabIndex = 15;
            this.gbScream.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(96, 85);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 21;
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(96, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 20;
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(96, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 19;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(444, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "label4";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(164, 223);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Aceptar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(275, 223);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PrincipalCostumerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 294);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbScream);
            this.Controls.Add(this.btnCashWithdrawal);
            this.Controls.Add(this.btntransfer);
            this.Controls.Add(this.btnbalanceInquiry);
            this.Controls.Add(this.btnchangePin);
            this.Name = "PrincipalCostumerForm";
            this.Load += new System.EventHandler(this.PrincipalCostumerForm_Load);
            this.gbScream.ResumeLayout(false);
            this.gbScream.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnchangePin;
        private System.Windows.Forms.Button btnbalanceInquiry;
        private System.Windows.Forms.Button btntransfer;
        private System.Windows.Forms.Button btnCashWithdrawal;
        private System.Windows.Forms.GroupBox gbScream;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}