﻿namespace CajeroBanco.GUI
{
    partial class ATM_recharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelClik = new System.Windows.Forms.Label();
            this.dg = new System.Windows.Forms.DataGridView();
            this.numericEdit = new System.Windows.Forms.NumericUpDown();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtnumeric = new System.Windows.Forms.NumericUpDown();
            this.cbxDenominatios = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // labelClik
            // 
            this.labelClik.AutoSize = true;
            this.labelClik.Location = new System.Drawing.Point(67, 107);
            this.labelClik.Name = "labelClik";
            this.labelClik.Size = new System.Drawing.Size(0, 13);
            this.labelClik.TabIndex = 45;
            // 
            // dg
            // 
            this.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg.Location = new System.Drawing.Point(54, 147);
            this.dg.Name = "dg";
            this.dg.Size = new System.Drawing.Size(316, 100);
            this.dg.TabIndex = 40;
            this.dg.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // numericEdit
            // 
            this.numericEdit.Location = new System.Drawing.Point(200, 113);
            this.numericEdit.Name = "numericEdit";
            this.numericEdit.Size = new System.Drawing.Size(106, 20);
            this.numericEdit.TabIndex = 44;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(220, 84);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 39;
            this.btnAdd.Text = "Agregar";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtnumeric
            // 
            this.txtnumeric.Location = new System.Drawing.Point(220, 58);
            this.txtnumeric.Name = "txtnumeric";
            this.txtnumeric.Size = new System.Drawing.Size(86, 20);
            this.txtnumeric.TabIndex = 37;
            // 
            // cbxDenominatios
            // 
            this.cbxDenominatios.FormattingEnabled = true;
            this.cbxDenominatios.Location = new System.Drawing.Point(67, 57);
            this.cbxDenominatios.Name = "cbxDenominatios";
            this.cbxDenominatios.Size = new System.Drawing.Size(121, 21);
            this.cbxDenominatios.TabIndex = 38;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(256, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 46;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(174, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 43;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(93, 11);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 41;
            this.btnDelete.Text = "Eliminar";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.button2_Click);
            // 
            // ATM_recharge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 312);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.labelClik);
            this.Controls.Add(this.numericEdit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dg);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.cbxDenominatios);
            this.Controls.Add(this.txtnumeric);
            this.Name = "ATM_recharge";
            this.Text = "ATM_recharge";
            this.Load += new System.EventHandler(this.ATM_recharge_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelClik;
        private System.Windows.Forms.DataGridView dg;
        private System.Windows.Forms.NumericUpDown numericEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.NumericUpDown txtnumeric;
        private System.Windows.Forms.ComboBox cbxDenominatios;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
    }
}