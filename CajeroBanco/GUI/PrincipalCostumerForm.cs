﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CajeroBanco.DAO;
using System.Xml;
using CajeroBanco.Entity;

namespace CajeroBanco.GUI
{
    public partial class PrincipalCostumerForm : Form
    {
        private Boolean isTrtransferencaiaFunds;
        private Boolean isChangePassword;
        private Boolean isCashWithdrawal;
        private Boolean isBalance;
         public string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private void defaultAll()
    {
           label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
         

    }

        public PrincipalCostumerForm(string ide)
        {
            InitializeComponent();
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            this.id = ide;
            label4.Text = id;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            label2.Visible = true;
            label1.Text = "PIN Antiguo";
            label2.Text = "PIN Nuevo";
            textBox1.Visible = true;
            textBox2.Visible = true;
            label3.Visible = false;
            textBox3.Visible = false;
            isTrtransferencaiaFunds = false;
            isChangePassword = true;
            isCashWithdrawal = false;
            isBalance = false;

        }

        private void PrincipalCostumerForm_Load(object sender, EventArgs e)
        {

        }

        private void btntransfer_Click(object sender, EventArgs e)
        {
            label1.Text = "Numero cedula";
            label2.Text = "Saldo";
            label1.Visible = true;
            label2.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            label3.Visible = false;
            textBox3.Visible = false;
            isTrtransferencaiaFunds = true;
            isChangePassword = false;
            isCashWithdrawal = false;
            isBalance = false;

             
        }

        private void btnCashWithdrawal_Click(object sender, EventArgs e)
        {
            textBox2.Visible = true;
            label2.Visible = true;
            label2.Text = "Saldo";
            label1.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox3.Visible = false;
            isTrtransferencaiaFunds = false;
            isChangePassword = false;
            isCashWithdrawal = true ;
            isBalance = false;
          

        }

        private void btnbalanceInquiry_Click(object sender, EventArgs e)
        {
            CostumerDAO cm = new CostumerDAO();
            label1.Visible = false;
            label2.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            label3.Text = "Su saldo es ";
            textBox3.Text = cm.balance(id);
            label3.Visible = true;
            textBox3.Visible = true;
            isTrtransferencaiaFunds = false;
            isChangePassword = false;
            isCashWithdrawal = false;
            isBalance = true ;
            
        }

        private void btn1_Click(object sender, EventArgs e)
        {


        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            validateNumer(sender,e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            validateNumer(sender, e);
        }

        private void validateNumer(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            validateNumer(sender, e);
           
        }

        private void textBox1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            validateNumer(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           
            CostumerDAO costumerDao = new CostumerDAO();

            if (isTrtransferencaiaFunds)
            {
                if (costumerDao.canTransfer(textBox1.Text, textBox2.Text, id))
                {

                    MessageBox.Show("Operecion realizada corectamente ");
                     
                }
                else
                {
                    MessageBox.Show("No se pudo realizar la operacion"
                , "Eror al validar campos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                }

                  defaultAll();
            }

        if (isCashWithdrawal)
	      {
            
              CostumerDAO cm = new CostumerDAO();
              if (cm.restarBill(textBox2.Text))
              {
                  if (cm.cashWithdrawal(textBox2.Text, id))
                  {
                      MessageBox.Show("Operecion realizada corectamente ");
                 
                  }
                  else
                  {
                      MessageBox.Show("fondos insuficientes"
                  , "jajaja ",
                      MessageBoxButtons.OK, MessageBoxIcon.Error);

                  }

                  defaultAll();
              }
              else
              {
                

                  MessageBox.Show("Error "
                      , "No hay billetes de esta denominacion" ,
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
           
              }
            
		
         
	      }

            if (isChangePassword)
	      {
              if (costumerDao.ChangePassword(textBox2.Text,textBox1.Text,id))
              {
                     MessageBox.Show("Pin cambiado correctamente");
            
              }
              else
              {
                  MessageBox.Show("PIN incorecto"
              , "Atencion",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);

              }
		      defaultAll();
          
	      }
           
   

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            defaultAll();

        }

    }
}
