﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CajeroBanco.DAO;

namespace CajeroBanco.Entity
{
    class Costumer :Person
    {
        //atributos 
        private string _accountNumber;
        private string _pass;
        private double _money;

      //contructor
        public Costumer(string id, string name , string lastName, string account , string pass,double money )
            :base(id,name,lastName)
        {
            this._accountNumber = account;
            this._pass = pass;
            this._money = money;


        }
        public Costumer()
        {

        }

        //
        public double Money
        {
            get { return _money; }
            set { _money = value; }
        }

        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }
    

        public string Pass
        {
            get { return _pass; }
            set { _pass = value; }
        }





        public override void changePass()
        {
           
        }
    }
}
